import time

import numpy as np
import torch
from torch import nn
from tqdm import tqdm


def acc(m1, m2):
    return (m1 == m2).mean()


def row_col_acc(m1, m2):
    m_same = m1 == m2
    return (m_same.all(axis=1) & m_same.all(axis=0)).mean()


def backward_share(m):  # todo: also handle negative entries above diagonal
    # mean of lower triangle under diagonal
    return np.tril(m, -1).mean()


def log_metrics(m_orig, m_other, tag):
    matrix_acc = acc(m_orig, m_other)
    matrix_row_col_acc = row_col_acc(m_orig, m_other)
    matrix_backward_share = backward_share(m_other)
    print(
        f"{tag} matrix: accuracy (elementwise): {matrix_acc}, "
        f"accuracy (share of indices with same row+column): {matrix_row_col_acc} "
        f"backward share: {matrix_backward_share}"
    )
    return matrix_acc, row_col_acc, matrix_backward_share


def create_matrix(
        size, density=0.8, diagonal_band=0, noise_prob=0.1, continuous_values=False
):
    m = np.random.rand(size, size) < density
    m = np.triu(m, 1) & (~np.triu(m, diagonal_band + 1))
    if noise_prob > 0:
        m ^= np.random.rand(*m.shape) < noise_prob
    if continuous_values:
        m = np.random.rand(size, size) * m
    return m


def shuffle_matrix(m):
    shuffled_indices = np.random.permutation(m.shape[0])
    return m[shuffled_indices][:, shuffled_indices]


def square_cost_fn(pre, post, weight, offset=0):
    return ((pre - post + offset).square() * weight).sum()


def abs_cost_fn(pre, post, weight, offset=0):
    return ((pre - post + offset).abs() * weight).sum()


def square_weighted_feedback_arc_cost_fn(pre, post, weight, offset=0):
    return (torch.relu(pre - post + offset).square() * weight).sum()


def gradient_matrix_sort(
        m, lr=0.0001, cost_fn=square_cost_fn, n_steps=100, device="cpu"
):
    """
    Reorder a matrix m according to the cost function cost_fn
    :param m: matrix to reorder (quadratic)
    :param lr: learning rate
    :param n_steps: number of optimization steps
    :param device: device to use (cpu or cuda)
    :param cost_fn: cost function to minimize (pre, post, weight) -> cost
    :return: reordered matrix
    """
    m = torch.tensor(m)

    nonzero_indices = m.nonzero()
    nonzero_values = m[nonzero_indices[:, 0], nonzero_indices[:, 1]].to(device)
    nonzero_indices = nonzero_indices.to(device).to(torch.int32)

    soft_indices = nn.Parameter(torch.rand(m.shape[0], device=device))
    optimizer = torch.optim.SGD([soft_indices], lr=lr)

    for i in tqdm(range(n_steps)):
        cost = optimization_step(
            cost_fn, nonzero_indices, nonzero_values, optimizer, soft_indices
        )
        if i == 0 or i == n_steps - 1:
            print(f"{i} cost: {cost.item()}")

    reordered_indices = soft_indices.cpu().argsort()
    reordered_matrix = m[reordered_indices][:, reordered_indices]
    return reordered_matrix.numpy()


@torch.compile
def optimization_step(
        cost_fn, nonzero_indices, nonzero_values, optimizer, soft_indices
):
    with torch.no_grad():  # normalize soft_indices
        soft_indices.data = soft_indices - soft_indices.min()
        soft_indices.data = soft_indices / soft_indices.max()
    optimizer.zero_grad()
    cost = cost_fn(
        soft_indices[nonzero_indices[:, 0]],
        soft_indices[nonzero_indices[:, 1]],
        nonzero_values.detach(),
    )
    cost.backward()
    optimizer.step()
    return cost


if __name__ == "__main__":
    torch.manual_seed(0)
    np.random.seed(0)

    n = 10000
    diagonal_band = n / 5 + 1
    original_matrix = create_matrix(
        n,
        diagonal_band=diagonal_band,
        density=0.8,
        noise_prob=0.1,
        continuous_values=False,
    )

    shuffled_matrix = shuffle_matrix(original_matrix)

    log_metrics(original_matrix, original_matrix, "original")
    log_metrics(original_matrix, shuffled_matrix, "shuffled")

    device = "cuda" if torch.cuda.is_available() else "cpu"
    offset = (diagonal_band / 2) / shuffled_matrix.shape[0]

    time_start = time.time()
    reordered_matrix = gradient_matrix_sort(
        shuffled_matrix,
        lr=0.0001,
        n_steps=100,
        device=device,
        # cost_fn=square_weighted_feedback_arc_cost_fn,
        cost_fn=lambda pre, post, weight: square_cost_fn(pre, post, weight, offset=offset),
        # cost_fn=lambda pre, post, weight: abs_cost_fn(pre, post, weight, offset=offset),
    )
    # reordered_matrix = SI_sort(shuffled_matrix)
    print(f"Total time elapsed: {time.time() - time_start}")
    log_metrics(original_matrix, reordered_matrix, "reordered")
