# gradient_matrix_sort

run `python3 gradient_matrix_sort.py` 

gradient_matrix_sort(...) function takes a matrix and returns a matrix with the same elements but sorted.